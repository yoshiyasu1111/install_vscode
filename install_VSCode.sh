#!/bin/bash

error_message() {
        echo "usage: sudo ./install_VSCode.sh [r|s|d] [i|u]" 1>&2
        echo "  r:      マイクロソフトのリポジトリ使用する。" 1>&2
        echo "  s:      snapを使用する。" 1>&2
        echo "  d:      debパッケージを使用する。" 1>&2
        echo "" 1>&2
        echo "  i:      インストール" 1>&2
        echo "  u:      アンインストール" 1>&2
        exit 1
}

install_by_repo() {
        if [ "$1" = "i" ]; then
                apt install -y curl apt-transport-https
                curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg
                install -o root -g root -m 644 microsoft.gpg /etc/apt/trusted.gpg.d/
                sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'
                apt update
                apt install -y code
        elif [ "$1" = "u" ]; then
                apt remove -y apt-transport-https curl code
                apt autoremove -y
                rm /etc/apt/sources.list.d/vscode.list
                rm /etc/apt/trusted.gpg.d/microsoft.gpg
        else
                error_message
        fi
}

install_by_snap() {
        if [ "$1" = "i" ]; then
                snap install --classic code
        elif [ "$1" = "u" ]; then
                snap remove code
        else
                error_message
        fi
}

install_by_deb() {
        if [ "$1" = "i" ]; then
                apt install -y curl
                curl -L https://go.microsoft.com/fwlink/?LinkID=760868 -o vscode.deb
                apt install -y ./vscode.deb
                rm ./vscode.deb
        elif [ "$1" = "u" ]; then
                apt remove -y code
        else
                error_message
        fi
}

if [ $# -ne 2 ]; then
        error_message
fi

if [ "$1" = "r" ]; then
        install_by_repo $2
elif [ "$1" = "s" ]; then
        install_by_snap $2
elif [ "$1" = "d" ]; then
        install_by_deb $2
else
        error_message
fi
